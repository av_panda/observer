#include <algorithm>
#include <functional>
#include <memory>
#include <vector>

using namespace std;
using namespace std::placeholders;

namespace kld {
namespace foundation {
template <typename T> class Observer {
public:
  virtual void OnUpdate(T) = 0;
};

template <typename Observer_> class Observable {
public:
  void AddObserver(const std::weak_ptr<Observer_> &observer) {
    if (observer.expired()) {
      return;
    };

    auto itr = std::find_if(observers_.begin(), observers_.end(),
                            [&](const std::weak_ptr<Observer_> &o) {
                              return o.lock() == observer.lock();
                            });

    if (itr == observers_.end()) {
      observers_.push_back(observer);
    }
  }

  void RemoveObserver(const std::weak_ptr<Observer_> &observer) {
    // There is no way to find the associated observer from the expired weak
    // pointer
    if (observer.expired()) {
      return;
    }

    // Remove the given observer and also use this chance to clean the dead
    // observers.
    observers_.erase(std::remove_if(observers_.begin(), observers_.end(),
                                    [&](const std::weak_ptr<Observer_> &o) {
                                      return o.expired() ||
                                             o.lock() == observer.lock();
                                    }),
                     observers_.end());
  }

  template <typename Callable_, typename... Args_>
  void Notify(Callable_ &&func, Args_ &&... args) {
    NotifyInternal(std::forward<function<void(std::shared_ptr<Observer_>)>>(
        std::bind(func, _1, args...)));
  }

  // Special cases for value observations.
  // TO-DO Add type check for Observer type only, however we will get compile
  // time error if different type is used.
  template <typename T> void NotifyValue(T data) {
    auto weakObserverItr = observers_.begin();
    // safe iteration while removing dead observers
    while (weakObserverItr != observers_.end()) {
      auto observer = weakObserverItr->lock();
      if (observer == nullptr) {
        weakObserverItr = observers_.erase(weakObserverItr);
        continue;
      }
      ++weakObserverItr;
      observer->OnUpdate(data);
    }
  }

protected:
  void NotifyInternal(function<void(std::shared_ptr<Observer_>)> func) {
    auto weakObserverItr = observers_.begin();
    // safe iteration while removing dead observers
    while (weakObserverItr != observers_.end()) {
      auto observer = weakObserverItr->lock();

      if (observer == nullptr) {
        weakObserverItr = observers_.erase(weakObserverItr);
        continue;
      }
      ++weakObserverItr;
      ExecuetInternal(
          std::forward<function<void(void)>>(std::bind(func, observer)));
    }
  }

  // NOT required, kept for future work for storing executable functions
  // execute at once.
  void ExecuetInternal(const function<void(void)> &func) {}

protected:
  std::vector<std::weak_ptr<Observer_>> observers_;
};

} // namespace foundation
} // namespace kld

