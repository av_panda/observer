#include "Observer.h"
#include <iostream>
#include <string>

using namespace kld::foundation;

class MyDataObserver : public Observer<int> {
public:
  virtual void OnUpdate(int i) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << i << std::endl;
  }
};

class Foo {
public:
  int data() {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    return 1;
  };
};

class MyObjectObserver : public Observer<Foo> {
public:
  virtual void OnUpdate(Foo o) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << o.data() << std::endl;
  }
};

class MyObjectObserverSecond : public Observer<Foo> {
public:
  virtual void OnUpdate(Foo o) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << o.data() << std::endl;
  }
};

class SystemObserver {
public:
  virtual void onSuccess(std::string s) {
    std::cout << __PRETTY_FUNCTION__ << ":" << s << std::endl;
  }
  virtual void onFailure() { std::cout << __PRETTY_FUNCTION__ << std::endl; }
};

class SystemEventProducer;

class SystemLogObserver : public Observer<std::string> {
public:
  virtual void onWriteSuccessLog(std::string s) {
    std::cout << __PRETTY_FUNCTION__ << ":" << s << std::endl;
  }
  virtual void onWriteFailure() {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
  }
  virtual void onWriteSender(std::shared_ptr<SystemEventProducer> producer) {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
  }

  virtual void OnUpdate(std::string s) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << s << std::endl;
  }
};

class SystemEventProducer
    : public Observable<SystemObserver>,
      public Observable<SystemLogObserver>,
      public Observable<MyDataObserver>,
      public enable_shared_from_this<SystemEventProducer> {
public:
  SystemEventProducer(std::weak_ptr<SystemObserver> systemObserver)
      : systemObserver_(systemObserver) {}

  void addLogObserver(std::weak_ptr<SystemLogObserver> systemLogObserver) {
    systemLogObserver_ = systemLogObserver;
    Observable<SystemLogObserver>::AddObserver(systemLogObserver_);
  }

  void addIntObserver(std::weak_ptr<MyDataObserver> intListetener) {

    Observable<MyDataObserver>::AddObserver(intListetener);
  }

  void init() {
    Observable<SystemObserver>::AddObserver(systemObserver_);
    Observable<SystemLogObserver>::AddObserver(systemLogObserver_);
  }

  void produce() {
    Observable<SystemObserver>::Notify(&SystemObserver::onSuccess, "hello");
    Observable<SystemObserver>::Notify(&SystemObserver::onFailure);

    Observable<SystemLogObserver>::Notify(&SystemLogObserver::onWriteSuccessLog,
                                          "hello");
    Observable<SystemLogObserver>::Notify(&SystemLogObserver::onWriteFailure);
    Observable<SystemLogObserver>::NotifyValue("hello");
    Observable<SystemLogObserver>::Notify(&SystemLogObserver::onWriteSender,
                                          shared_from_this());
    Observable<MyDataObserver>::NotifyValue(99999);
  }

  void print() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

private:
  std::weak_ptr<SystemObserver> systemObserver_;
  std::weak_ptr<SystemLogObserver> systemLogObserver_;
};

class SpecialSystemObserver : public SystemObserver {
public:
  void onSuccess(std::string s) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << s << std::endl;
  }
  void onFailure() override { std::cout << __PRETTY_FUNCTION__ << std::endl; }
};

int main() {

  auto systemObserver = make_shared<SystemObserver>();
  auto producer = make_shared<SystemEventProducer>(systemObserver);
  producer->init();
  auto logObserver = make_shared<SystemLogObserver>();
  producer->addLogObserver(logObserver);
  producer->produce();

  auto specialObserver = make_shared<SpecialSystemObserver>();

  auto specialProducer = make_shared<SystemEventProducer>(specialObserver);

  auto integerObserver = make_shared<MyDataObserver>();
  specialProducer->addIntObserver(integerObserver);

  specialProducer->init();
  specialObserver.reset();
  specialProducer->produce();
  specialProducer->produce();

  specialProducer->produce();
  specialProducer->Observable<MyDataObserver>::RemoveObserver(integerObserver);
  specialProducer->produce();

  Observable<Observer<Foo>> myobj;
  auto obj1 = make_shared<MyObjectObserver>();
  auto obj2 = make_shared<MyObjectObserver>();
  auto obj3 = make_shared<MyObjectObserverSecond>();
  myobj.AddObserver(obj1);
  myobj.AddObserver(obj2);
  myobj.AddObserver(obj3);
  myobj.AddObserver(obj1);
  myobj.AddObserver(obj1);
  myobj.AddObserver(obj2);
  myobj.NotifyValue(Foo());

  Observable<Foo> myfunc;
  auto foo1 = make_shared<Foo>();
  auto foo2 = make_shared<Foo>();
  auto foo3 = make_shared<Foo>();
  myfunc.AddObserver(foo1);
  myfunc.AddObserver(foo2);
  myfunc.AddObserver(foo3);
  myfunc.Notify(&Foo::data);

  return 1;
}
